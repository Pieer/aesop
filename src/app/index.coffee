angular.module('aesop',[
  'ngAnimate', 'ngTouch', 'ngSanitize', 'angular-images-loaded', 'infinite-scroll',
  'restangular','ui.router','duScroll', 'slick', 'ngDialog', 'socialLinks', 'tinacious.fluidVid','angulartics', 'angulartics.google.analytics'
])
.config ($stateProvider, $locationProvider, $urlRouterProvider, RestangularProvider, $provide, $analyticsProvider) ->
  # Patch for ios9
  $provide.decorator '$browser', [
    '$delegate'
    '$window'
    ($delegate, $window) ->
      $analyticsProvider.firstPageview(true)
      $analyticsProvider.withAutoBase(true)

      isIOS9UIWebView = (userAgent) ->
        /(iPhone|iPad|iPod).* OS 9_\d/.test(userAgent) and !/Version\/9\./.test(userAgent)

      applyIOS9Shim = (browser) ->
        pendingLocationUrl = null
        originalUrlFn = browser.url

        clearPendingLocationUrl = ->
          pendingLocationUrl = null
          return

        browser.url = ->
          if arguments.length
            pendingLocationUrl = arguments[0]
            return originalUrlFn.apply(browser, arguments)
          pendingLocationUrl or originalUrlFn.apply(browser, arguments)

        window.addEventListener 'popstate', clearPendingLocationUrl, false
        window.addEventListener 'hashchange', clearPendingLocationUrl, false
        browser

      if isIOS9UIWebView($window.navigator.userAgent)
        return applyIOS9Shim($delegate)
      $delegate
  ]

  window.mediaServer = 'http://d1rqq2np2rwv49.cloudfront.net'
  $locationProvider.hashPrefix('!')

  lg = window.localStorage['language']
#  baseUrl = window.serverName = 'http://taxonomyofdesign.com'
  #TODO replace by live url
  baseUrl = window.serverName = 'http://taxonomy-staging.pollen.com.au'
  if lg
    baseUrl += '/'+lg
  else
    notPrivateBrowsing = try
      !!localStorage.getItem('oldVisitor')
    catch e
      console.log e

    baseUrl += '/en-us' unless notPrivateBrowsing

  RestangularProvider.setDefaultHttpFields
    cache: true

  RestangularProvider.setBaseUrl(baseUrl+'/api/')

  gridTemplate =
    'content@home':
      templateUrl: "app/components/grid/grid.html"

  createConf = (page)->
    sections = ['intro','header','content','footer']
    base =
      'main':
        templateUrl: "app/main/main.html"
    for section in  sections
      t = section
      if section is 'content'
        t = if page is 'store' then 'store' else 'grid'
      base[section+'@'+page] =
        templateUrl: 'app/components/'+t+'/'+t+'.html'
    base

  $stateProvider
    .state "home",
      url: '/',
      abstract: true,
      views: createConf('home')

    .state "home.all",
      url: "^/",
      views: gridTemplate

    .state "home.stores",
      url: "^/stores",
      views: gridTemplate

    .state "home.designers",
      url: "^/designers",
      views: gridTemplate

    .state "home.features",
      url: "^/features",
      views: gridTemplate

    .state "home.materials",
      url: "^/materials",
      views: gridTemplate

    .state "home.quotes",
      url: "^/quotes",
      views: gridTemplate

    .state "home.films",
      url: "^/films",
      views: gridTemplate

    .state "store",
      url: "^/store?store&section",
      views: createConf('store')

    .state "designer",
      url: "^/designer?designer",
      views: createConf('designer')

    .state "search",
      url: "^/search?q",
      views: createConf('search')

  $urlRouterProvider.otherwise '/'

