angular.module 'aesop'
  .controller 'MainCtrl', ($scope, $rootScope, $state, $window, Restangular, ngDialog, $timeout) ->
    $rootScope.loading = 'page-loading'
    $rootScope.isDesktop = $window.innerWidth >= 767

    $rootScope.isTouch = !!('undefined' != typeof document.documentElement.ontouchstart)
    $rootScope.transitionDelay = if $rootScope.isDesktop then 500 else 0
    $rootScope.translations={}
    getTranslation = ->
      # Temp
      $rootScope.translations={}
#      $rootScope.translations.philosophy_close = 'Close'
      Restangular.one('site').get().then (response) ->
        $rootScope.translations = response
        $timeout ->
          $rootScope.loading = 'page-ready'
        , 1000

        $timeout ->
          $rootScope.loading = ''
        , 5000

    $rootScope.$on 'refreshTranslation', ->
      Restangular.setBaseUrl window.serverName+'/'+window.localStorage['language']+'/api/'
      getTranslation()

    getTranslation()

    # Bind resize and scroll events in the main to prevent memory leek
    # Debounce maybe necessary
    angular.element($window).on 'resize', $window._.debounce((->
      if $rootScope.isDesktop isnt ($window.innerWidth >= 768)
        $rootScope.isDesktop = !$rootScope.isDesktop
        $scope.$broadcast 'updateLayout'
      return
    ), 500)

    if $rootScope.isDesktop
      angular.element($window).on 'scroll', ->
        $scope.$broadcast 'scroll::scroll'
        return

    $rootScope.$on 'showIntro', ->
      ngDialog.open
        template: 'philosophy'
        scope: $scope
        className: 'dialog philosophy'
      return

    return
