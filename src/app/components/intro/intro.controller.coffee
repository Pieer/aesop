angular.module 'aesop'
  .filter 'splitTranslation', ->
    (input) ->
      if input
        input = for copy in input.split(/(?:\n\r)/g)
          '<span class="line">'+copy.replace(/(?:\r\n|\r|\n)/g, '')+'</span>'
        return input.join('')
  .controller 'IntroCtrl', ($scope, $rootScope) ->

    $scope.button = $rootScope.translations.philosophy_close or 'close'

    return