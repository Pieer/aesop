angular.module "aesop"
.controller "SearchCtrl", ($scope, $rootScope, $state, $window) ->

  $scope.closeSearch = ->
    $scope.searchInput = null
    if($rootScope.page == 'page-search')
      $window.history.back()

  $rootScope.$on 'removeResult', ->
    $scope.searchInput = null

  $scope.search = ->
    $scope.toggleNav()
    if $scope.searchInput
      $state.go('search',{q:$scope.searchInput}, false)
    return

  return