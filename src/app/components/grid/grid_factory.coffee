app = angular.module "aesop"
app.factory 'layout', ($state) ->
  addBrick: (el) ->
    # No links on show page
    if @page is 'store'
      el.image_duotone = null
    else
      # Different structure on the backend for designers data
      if el.element_type is 'designer' and @page isnt 'designer'
        el.ref = $state.href 'designer',
          'designer': el.element_id
      else
        el.ref = $state.href 'store',
          'store': el.related_store
          'section': el.element_type

    el.size =  's' + el.size
    el.isImage = el.image_duotone? and el.image_duotone isnt ''

    el

  createLayout: (settings) ->
    @page = settings.page
    layout = settings.layout
    items = settings.items
    mainWrapper = settings.mainWrapper
    orderedSerie = []
    bricks = []
    bricksWrapper =
      size: ''
      bricks: []

    if layout?
      if @page is 'store'
        for order in layout
          for item, num in items
            if(item.size is order and not item.used)
              orderedSerie.push item
              item.used = true
              break
      else
        # TODO: reorder items server side
        # clean array
        serie = layout.filter (i) ->
          return i isnt 'r' and i isnt 'c'

        # flat array
        serie = [].concat ([].concat serie...)...

        # Reorder bricks to match the order of the layout
        for order, ind in serie
          for item, num in items
            # Check if the brick size match the size of the layout
            if(item.size is order and not item.used)
              orderedSerie.push item
              item.used = true
              break

      # mainWrapper[bricksWrapper[bricks[brick]]]
      # Bricks are push to the main wrapper
      # If they are arrays, then they are push in a wrapper array
      # It can go 2 level deep

      # Reset reverse order
      reverse = false
      for comb, index in layout

        clear = if((layout[0] is 'r' and index is 1) or index is 0 or layout[index-1] is 'c') then ' clearLeft' else ''
        if comb is 'r' or comb is 'c'
          reverse = true if comb is 'r'
        else
          wwSize = 0
          # Check if the object is a brick or a wrapper
          if comb.constructor is Array
            for el in comb
              if el is 'r'
                reverse = true
              else
                wSize = 0
                # Check if the object is a brick or a wrapper
                if el.constructor is Array
                  for el2 in el
                    wwSize = el2 if el2 > wwSize
                    wSize = el2 if el2 > wSize
                    if orderedSerie.length > 0
                      bricks.push @addBrick(orderedSerie.shift())
                else
                  wwSize = el if el > wwSize
                  wSize = el
                  if orderedSerie.length > 0
                    bricks.push @addBrick(orderedSerie.shift())

                className = 's' + wSize
                if reverse
                  className = 'reverse ' + className
                  reverse = false
                bricksWrapper.bricks.push
                  bricks: bricks
                  size: className
                bricks = []
          else
            if orderedSerie.length > 0
              bricks.push @addBrick(orderedSerie.shift())
              wwSize = comb
              bricksWrapper.bricks.push
                bricks: bricks
                size: 's' + comb
              bricks = []
          if reverse
            bricksWrapper.size = 'reverse s' + wwSize
            reverse = false
          else
            bricksWrapper.size = 's' + wwSize

          # clear left on every layer
#          console.log(clear)
          bricksWrapper.size = bricksWrapper.size + clear
          mainWrapper.push bricksWrapper
          bricksWrapper =
            size: ''
            bricks: []

    else
      for comb in items
        bricks.push @addBrick(comb)
        bricksWrapper.bricks.push
          bricks: bricks
          size: 's' + comb.size
        bricks = []
        bricksWrapper.size = 'rest ' + comb.size
        mainWrapper.push bricksWrapper
        bricksWrapper =
          size: ''
          bricks: []

    mainWrapper
