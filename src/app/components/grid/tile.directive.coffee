angular.module "aesop"

.directive 'tile', ($rootScope, Restangular, $window) ->
  link: (scope, element, attrs) ->
    imgFolder = window.mediaServer + '/media/'
    ext = if $rootScope.webP then '.jpg' else '.webp'
    ext = '.jpg'
    brick = JSON.parse(attrs.ngBrick)
    if brick.image_duotone
      image = imgFolder+brick.image_duotone
      colorImage = imgFolder+brick.image_colour
    else
      image = imgFolder+brick.image_colour
      colorImage = null

    colorOverlay =
      'background-image': 'url('+colorImage+')'
      opacity: 0

    scope.overlay = ''
    scope.overlayDuotone =
      'background-image': 'url('+image+')'
    scope.link = image
    scope.brickid = brick.id
    scope.title = brick.title
    scope.subtitle = brick.subtitle
    # scope.isFilm = brick.element_type is 'film'
    scope.isFilm = scope.$parent.isMovieSection || brick.element_type is 'film'
    scope.element_type = brick.element_type
    scope.socialShare = scope.element_type is 'store' or scope.element_type is 'designer'

    scope.stop = ($event) ->
      $event.stopPropagation() if ($event.stopPropagation)
      $event.preventDefault() if ($event.preventDefault)
      $event.cancelBubble = true
      $event.returnValue = false

    scope.ref = if scope.isFilm then null else brick.ref
    scope.id = attrs.ngId
    scope.video = 0

    scope.open = (item, $event) ->
      # console.log(scope.isFilm, item, $event.currentTarget, item.target)
      if scope.isFilm
        if scope.$parent.isDesktop
          $rootScope.$emit 'openVideoGallery', scope.brickid
        else
          el = scope.$parent.findVideosFromId(scope.brickid)
          Restangular.all(el.video.element_type).get(el.video.element_id).then (response) ->
            scope.video = response.video

      true

    scope.loadSharingData = ->
      if scope.isFilm
        fv = scope.$parent.findVideosFromId(scope.brickid)
        el = fv.video
      else
        el = brick

      Restangular.all(el.element_type).get(el.element_id).then (response) ->
        scope.share = window.serverName+'/share/store/'+response.id+'/?image=' +
                      window.mediaServer+'/media/inspire_uncropped/'+response.inspire_photo1
        scope.photo = window.mediaServer+'/media/inspire_uncropped/'+response.inspire_photo1
        response.video_id = response.video
        scope.currentVideo = response

    scope.mail = ($event) ->
      scope.stop($event)
      if scope.isFilm
        fv = scope.$parent.findVideosFromId(scope.brickid)
        vid = fv.video

        if(vid)
          Restangular.all(vid.element_type).get(vid.related_store).then (response) ->
            $window.open('mailto:?subject=Aesop '+scope.title+'&body=https://vimeo.com/'+response.video
              ,'Aesop '+scope.title,'left=20,top=20,width=500,height=500,toolbar=1,resizable=0')
      else
        $window.open('mailto:?subject=Aesop '+scope.title+'&body='+window.serverName+'/'+scope.ref
        ,'Aesop '+scope.title,'left=20,top=20,width=500,height=500,toolbar=1,resizable=0')
      false

    if brick.image_duotone and not $rootScope.isTouch
      element.on 'mouseleave', ->
        colorOverlay.opacity = 0
        scope.overlay = colorOverlay
        scope.$digest()
        return

      element.on 'mouseenter', ->
        colorOverlay.opacity = 1
        scope.overlay = colorOverlay
        scope.$digest()
        return

  templateUrl: 'app/components/grid/tile.html'
  restrict: 'EA'
  transclude: true
  replace: true
  scope:
    ngBrick: '@'
