angular.module "aesop"
.controller "GridCtrl", ($scope, $rootScope, Restangular, $stateParams,  $state, $window, $document, $timeout, layout, ngDialog, $filter) ->
  combinations = null
  layouts = []
  mainWrapper = []
  desktopLayouts = [
    [6,[0,0],1,3,'c',[1,[0,0],3],4,2],
    ['r',[[1,1],5],[[0,0],4],[2,0],[0,1],[3,[0,0]]]
    [[3,[0,0],[0,1],[1,0]],6,3,1]
    [[6,[3,1,0,0],2],1,1,4,3]
    [6,1,[0,0],3,2,[0,1,0],6]
    [4,[1,[0,0],3],2,2,[3,'r',1,[0,0]],4]
    [[5,0,0],3,1,[0,2],4,0,0]
    [[2,0,0],[1,2],4,1,1,1,1,1]
    [6,3,1,1,['r',1,[0,0],3],4,1,0,0]
    [[1,1,1],2,6,3,1,1]
    [5,3,'r',2,2,1,3]
    [3,'r',4,'r',2,2,1,3,1,1]
    [1,1,1,'r',5,4,2,2,[1,1],4,2]
    [[1,0,0],4,1,[0,0],3,1,3,1,1]
    [[2,2,3],3,'r',2,2,[0,0],3]
    ['r',[3,2,2],2,4,3,[0,0]]
    #store
    [6,3,3]
    [[3,3],6]
    [[5,3],6,6]
    [[6,6],3,5]
    [[3,3],6,6,3,3]
    [6,3,3,[3,3],6]
    [5,6,6]#not used
    #materials
    [1,3,1,1]
    [3,[0,0],3]
    [1,1,3,[0,0]]
    [[0,1],[1,0],[0,1],[1,0],[0,1]]
    [[0,0],1,[0,0],1,[0,0]]
    [1,[0,0],1,[0,0],1]
    [1,1,1,1,1]
    [1,1,3,1]
    [[1,0],[0,1],[1,0],[0,1],[1,0]]
    [3,[0,0],[0,0],[0,0]]
    [0,0,'r',3,'r',1,[0,1],1,'r',1,'r',1,'r',1,0]
    [[0,0,1],0,0,'r',1,'r',1,3,'r',1,'r',1,0,0,]
    [[0,0],1,1,0,0,'r',1,'r',1,1,[0,0],1,0,0]
    [[0,0,0],[0,1],1,1,1,0,0,0]
    [1,[0,0],[0,0],1,1]
    [1,1,1,1,[0,0]]
    [1,1,1,1,1]
    [1,[0,0],1,1,1]
    [0,0,0,0,0]
    [[0,1,0],[0,1,0],1,1,1,1,[0,0],1]
    [[1,0,0],[1,1],[0,1,0],[0,0,1],1,1]
    #features
    [1,3,'r',4,2,3,1,3,1]
    [[4,1,1,3],1,1,1,2,4,1,3]
    [[4,1,'r',2,1],[2,1],3,4,3,1]
    [3,'r',4,'r',2,[1,2],2,1,3,3,1,1]
    [1,1,3,'r',2,[3,4],[1,2],'r',[2,1],1,1] #fail
    [1,1,3,1,3,1,1,1]
    [2,[1,1,3],2,1,1]
    [[3,1,1],4,2]
    [2,4,3,3]
    [[1,1],[1,1,3],4]
    [[1,1],2,1,3,3,1]
    [[1,1],2,[1,1],2,[1,1]]
    [4,[1,1],2,2]
    [2,[3,3],2,2]
    #film
    [[5,5],6,6,6]
    [[5,5],6,6,6]
  ]
  mobileLayouts = [
    [6,3,[0,0],1,3,4,2,0,0,1]
    [1,1,5,0,0,4,2,0,1,0,3,0,0]
    [3,[0,0],1,0,0,11,6,3]
    [6,3,1,0,0,2,1,1,4,3]
    [6,1,0,0,3,2,1,0,0,6]
    [4,1,0,0,3,2,2,3,1,0,0,4]
    [5,0,0,3,2,1,0,0,4,0]
    [2,0,0,1,2,1,1,4,1,1]
    [6,3,1,1,1,0,0,3,4,1,0,0]
    [1,1,2,1,1,6,3]
    [5,3,2,2,3]
    [3,4,2,2,3,1,1]
    [1,1,5,4,2,2,4,2,1,1]
    [1,0,0,4,1,0,0,3,3,1,1]
    [2,2,3,3,2,2,0,0,3]
    [3,2,2,2,0,0,4,3]
    #store
    [6,3,3]
    [3,3,6]
    [5,3,6,6]
    [6,6,3,5]
    [3,3,6,6,3,3]
    [6,3,3,3,3,6]


    #materials
    [1,1,3]
    [3,3,0,0]
    [3,1,1,0,0]
    [0,0,1,1,0,0,1,1,0,0,1]
    [0,0,1,[0,0],1,[0,0]]
    [1,[0,0],1,[0,0]]
    [1,1,1,1]
    [1,1,3]
    [1,0,0,1,1,0,0,1,1]
    [3,0,0,0,0,0,0]
    [0,0,3,1,0,0,1,1,1,1]
    [0,0,1,[0,0],1,1,3,1,1,0,0]
    [[0,0],1,1,0,0,1,1,1,[0,0],1,[0,0]]
    [0,0,0,0,1,1,1,1,0,0]
    [1,[0,0],[0,1],[0,1]]
    [1,1,1,1,0,0]
    [1,1,1,1]
    [1,[0,0],1,1]
    [0,0,0,0]
    [0,0,1,1,0,0,1,1,1,1,1,0,0]
    [1,1,[0,0],1,1,0,0,0,0,1,1]
    #features
    [3,4,2,1,1,3,3]
    [4,1,1,3,1,1,4,2,1,1,3]
    [4,1,1,2,2,1,1,4,3,3]
    [3,1,1,4,2,2,2,1,1,3,3]
    [2,1,1,3,3,4,2,2,1,1,1,1]
    [1,1,3,1,1,3,1,1]
    [2,1,1,2,1,1,3]
    [4,2,1,1,3]
    [4,3,3]
    [1,1,1,1,4,3]
    [2,1,1,3,3]
    [1,1,2,1,1,2,1,1]
    [4,1,1,2,2]
    [3,3,2,2]
    #film
    [[5,5],6,6,6]
    [[5,5],6,6,6]
  ]

  pagination = 0
  isDesignerPage = false

  if $rootScope.isDesktop
    $scope.imageHeight = $rootScope.mediaHeight

  init = ->
    # Display different layout on mobile and desktop
    layouts = if $rootScope.isDesktop then desktopLayouts else mobileLayouts
    pagination = 0
    $scope.limit = 0
    # Create wall after switch to mobile or desktop (not working yet)
    if combinations?
      $scope.getBrick()
      $scope.$digest()
    return

  $scope.mainWrapper = null
  $scope.scrollEnable = true
  $rootScope.storeLocation = ''
  $scope.limit = 0
  # On mobile we load more stuff in advance because no scrolling event
  $scope.distance = if $rootScope.isTouch then 800 else 0
  $scope.videos = []
  $scope.isMovieSection = false
  $scope.carouselReady = false
  intialLoad = false

  $scope.getBrick = ->
    nbComb = combinations.combinations.length
    if nbComb
      if pagination < nbComb

        row = combinations.combinations[pagination]
        layer = if row.combination_id is 100 then null else layouts[row.combination_id-1]
        mainWrapper = layout.createLayout
          items: row.items
          layout: layer
          page: sectionUrl
          mainWrapper : mainWrapper

        $scope.limit += row.items.length
        $scope.mainWrapper = mainWrapper

        pagination++
        if $scope.scrollEnable
          $scope.showMore = pagination isnt nbComb and row.combination_id isnt 100
        if($rootScope.page == 'page-search')
          $scope.showClear = pagination is nbComb or row.combination_id is 100
    return

  $scope.initInfinite = ->
    $rootScope.$emit 'stickyFooter'
    $scope.showMore = false
    $scope.scrollEnable = false
    return

  if $state.current.name.indexOf("home")>=0
    $scope.section = section = $state.current.name.replace('home.','')
  else
    isDesignerPage = true
    $scope.section = section = $state.current.name + 's' #temp

  $scope.showMore = false
  $scope.showFilters = $state.current.name isnt 'home.all'

  sectionUrl = ''
  # Home and all get the root of the api
  sectionUrl = section if section isnt 'all' and section isnt 'home'

  offUpdateLayout = $rootScope.$on('updateLayout', init)
  $rootScope.storeName = null
  getContent = ->
    $scope.videos = []
    if isDesignerPage
      $scope.gridClass = 'designer-container'
      # Manage server API irregularity
      if $stateParams.hasOwnProperty('designer')
        Restangular.all('designer').get($stateParams.designer).then (response) ->
          $rootScope.storeName = response.title
          $rootScope.page = 'page-designer'
          combinations = response.images
          layers = 6
          while layers--
            $scope.getBrick()
      else
        Restangular.all('search').get('?q='+$stateParams.q).then (response) ->
          $rootScope.storeName = response.title
          $rootScope.page = 'page-search'
          $scope.page = 'page-search'
          $scope.showClear = !$scope.showMore
          combinations = response
          if response.combinations.length
            $scope.getBrick()
          else
            $scope.mainWrapper = []

    else
      Restangular.all('').get(sectionUrl).then (response) ->
        $rootScope.page = 'page-grid'
        combinations = response

        if(sectionUrl is 'films')
          $rootScope.page = 'page-grid page-videos'
          $scope.isMovieSection = true
          for cb in combinations.combinations
            for it in cb.items
              it.colorImage = window.mediaServer + '/media/'+it.image_colour
              $scope.videos.push(it)

        # Get more items because some categories are shorter
        layers = switch sectionUrl
          when 'features', 'stores', 'films' then 4
          when 'materials', 'quotes' then 6
          else 2

        while layers--
          $scope.getBrick()

  getContent()
  init()


  offRefreshTranslation = $rootScope.$on 'refreshTranslation', ->
    $scope.mainWrapper = null
    $scope.scrollEnable = true
    pagination = 0
    $scope.limit = 0
    combinations = null
    mainWrapper = []
    getContent()
    false

  $scope.findVideosFromId = (num)->
    position = -1
    found = false
    {
      video: $filter('filter')($scope.videos, (d) ->
        position++ unless found
        found = true if d.id is num
        d.id is num
      )[0],
      position: position
    }


  offOpenVideoGallery = $rootScope.$on 'openVideoGallery', (event, num) ->
    fv = $scope.findVideosFromId(num)
    vid = fv.video
    vcount = fv.position

    if(vid)
      Restangular.all(vid.element_type).get(vid.related_store).then (response) ->
        $scope.currentVideo = response
        $scope.count = 0
        if(vid.element_type is 'store')
          $scope.currentVideo.video_id = response.video

        getShare(response,vid.element_type)

        if $rootScope.isDesktop
          ngDialog.open
            template: 'videogallery'
            scope: $scope
            className: 'dialog'
          setTimeout ->
            $scope.count = vcount
            $scope.carouselReady = true
          , 10
        else
          $scope.count = parseInt num
        true
    else
      console.log('no video!?')
    true

#  $scope.mail = ->
#    $window.open('mailto:?subject=Aesop '+$scope.currentVideo.title+'&body=https://vimeo.com/'+$scope.currentVideo.video_id,
#      'Aesop '+$scope.currentVideo.title,'left=20,top=20,width=500,height=500,toolbar=1,resizable=0')
#    false

  getShare = (response, el)->
    if(el is 'store')
      $scope.share = window.serverName+'/share/store/'+response.id+'/?image='+window.mediaServer+'/media/inspire_uncropped/'+response.inspire_photo1
      $scope.photo = window.mediaServer+'/media/inspire_uncropped/'+response.inspire_photo1
    else
      $scope.share = null
      $scope.photo = null

  $scope.currentVideo = null
  $scope.showcaption = true

  $scope.beforeChange = ->
    $scope.currentVideo = null
    $scope.showcaption = false
    $scope.$apply()

  $scope.afterChange = (num) ->
    if(num< $scope.videos.length)
      $scope.currentVideo = null
      $scope.$apply()
      vid = $scope.videos[num]

      Restangular.all(vid.element_type).get(vid.element_id).then (response) ->
        setTimeout( ->
          $scope.currentVideo = response
          if(vid.element_type is 'store')
            $scope.currentVideo.video_id = response.video
          $scope.$apply()
        , 0)

        setTimeout( ->
          $scope.appear = true
          $scope.showcaption = true
          $scope.$apply()
        , 1000)

        getShare(response,vid.element_type)
        false

  $scope.clearSearch = ->
    $scope.showClear = false
    if($rootScope.page == 'page-search')
      $window.history.back()
    $rootScope.$emit('removeResult')

  # Action that don't need the data to be present on the page
  # and need to be synchronize with the page transition
  if $rootScope.isDesktop and not $rootScope.isTouch
    # When dom ready
    $timeout ->
      # Offset scroll to top of the header
      $scope.offset = angular.element('#fixHeader').height() + angular.element('.fix-header-top').height()
      gotoSection = angular.element('.grid-container')

      # Go back to the top if the header is retracted
      if gotoSection.length > 0 and $document.scrollTop() >= $scope.offset
        # Animation perform before the content opacity is 1
        $document.duScrollTo(gotoSection,$scope.offset, 100)

      return
      # ! timing relative to transition
    , $rootScope.transitionDelay, false


  $scope.$on '$destroy', ->
    offOpenVideoGallery()
    offRefreshTranslation()
    offUpdateLayout()

  return
