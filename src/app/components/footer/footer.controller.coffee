angular.module "aesop"
  .controller "FooterCtrl", ($scope, $rootScope, $http, $stateParams, $timeout) ->

    $scope.form = {}
    $scope.error = false

    $rootScope.$on '$stateChangeStart', ->
      $scope.footerState = ''
    $rootScope.$on 'stickyFooter', ->
      $scope.footerState = 'sticky readyToStick'
      $timeout ->
        $scope.footerState = 'sticky'
        $scope.$digest()
      , 1000, false

    send = (formData) ->
      lang = 'en-us'

      if window.localStorage['language']
        lang = window.localStorage['language']
      $http.post(window.serverName+'/'+lang+'/api/subscribe/', formData)
      .success (data) ->
        $scope.success = $rootScope.translations.label_subscribe_success
        $scope.form.email_address = ''
      .error (data) ->
        $scope.error = $rootScope.translations.label_subscribe_failure

      return

    $scope.subscribe = (e) ->
      if @form.email_address is '' or !@form.hasOwnProperty('email_address')
        $scope.error = $rootScope.translations.label_subscribe_emailinvalid
      else
        if @form.email_address.$valid
          $scope.error = $rootScope.translations.label_subscribe_emailinvalid
        else
          $scope.error = ''
          send(@form)

      return false
