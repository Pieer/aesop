#app = angular.module "aesop"
#app.factory 'webPService', ($q) ->
#  isWebP: ->
#    deferred = $q.defer()
#    hasWebP = false
#    img = new Image
#
#    img.onload = ->
#      hasWebP = ! !(img.height > 0 and img.width > 0)
#      deferred.resolve hasWebP
#      return
#
#    img.onerror = ->
#      hasWebP = false
#      deferred.reject hasWebP
#      return
#
#    img.src = 'data:image/webp;base64,UklGRjIAAABXRUJQVlA4ICYAAACyAgCdASoCAAEALmk0mk0iIiIiIgBoSygABc6zbAAA/v56QAAAAA=='
#    deferred.promise

# Check webP support (code to include on the main controller)
#    webPSupport = webPService.isWebP()
#    webPSupport.then (->
#      $rootScope.webP = true
#      return
#    ), ->
#      $rootScope.webP = false
#      return

#app.factory 'retinaService', ->
#  isRetina: ->
#    dpr = window.devicePixelRatio or window.screen.deviceXDPI / window.screen.logicalXDPI or 1
#    return dpr > 1
#$rootScope.retina = retinaService.isRetina()