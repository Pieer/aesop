angular.module "aesop"
  .controller "HeaderCtrl", ($scope, $rootScope, $state, $stateParams, $document, $timeout, $window) ->
    nav = ['all','stores','designers','features','materials','quotes', 'films']
    # Todo replace with api laguage data
    $scope.lang = [
      abbr:'日本語'
      code: 'ja'
    ,
      abbr:'한국어'
      code: 'ko'
    ,
      abbr:'简体中文'
      code: 'zh-cn'
    ,
      abbr:'繁體中文'
      code: 'zh-tw'
    ,
      abbr:'English'
      code: 'en-us'
    ]

    activeMobileNav = false
    headerHeight = 60
    subHeaderHeight = 88
    topPos = 110
    scrollTop = 0
    $rootScope.nav = []

    $scope.searchInput = null
    $scope.showPhilosophy = true
    $scope.mainHeader = false
    $scope.offset = if $rootScope.isDesktop then 120 else 78
    $scope.isMobile = !$rootScope.isDesktop

    if angular.equals {}, $stateParams
      $scope.mainHeader = true
      topPos = subHeaderHeight
      $rootScope.nav = nav
    else
      # Designer, store and search pages
      $rootScope.nav = nav unless $rootScope.isDesktop
      switch $state.current.name
        when 'designer'
          $timeout ->
            $document.duScrollTo()
          , $rootScope.transitionDelay, false
        when 'search'
          $scope.isSearch = true
          $scope.searchInput = $stateParams.q
          $scope.storeName = $stateParams.q
          $rootScope.state += ' search'
          $scope.showPhilosophy = false
        when 'store'
          $scope.showPhilosophy = false

    $scope.toggleNav = ->
      activeMobileNav = !activeMobileNav
      $scope.navState = if activeMobileNav then 'nav-open' else ''
      return

    $scope.philosophy = ->
      $state.transitionTo 'home.all'
      $rootScope.$broadcast 'showIntro'
      return

    $scope.switchLang = (code) ->
      window.localStorage['language'] = code
      $rootScope.$broadcast 'refreshTranslation'
      return

    $scope.mail = ->
      $window.open('mailto:?subject='+$rootScope.translations.title+'&body='+$rootScope.translations.sharing_message,
        $rootScope.translations.title,'left=20,top=20,width=500,height=500,toolbar=1,resizable=0')
      return

    # Scroll
    isFixed = $scope.fixMe
    $scope.activeFixHeader = ''
    deployed = false
    oldPos = 0
    logoPosition = (logoHeight) ->
      logoPos = if scrollTop < logoHeight and not $rootScope.isTouch then logoHeight - scrollTop else 0
      transform: 'translate3d(0,'+logoPos+'px,0)'

    animHeader = ->
      $scope.activeFixHeader = if scrollTop > 0 then 'active' else ''
      $scope.transformLogo = logoPosition headerHeight
      $scope.transformSubLogo = logoPosition subHeaderHeight

      deployed = scrollTop > topPos + 18
      $scope.$digest()
      return

    checkPosition = ->
      scrollTop = $document.scrollTop()
      # Check speed to avoid gap when scrolling fast
      speed = if speed > 5 then 5 else scrollTop - oldPos
      oldPos = scrollTop
      isFixed = if scrollTop > topPos - speed then 'active' else ''

      # Digest only when necessary
      needDigest = false
      if $scope.fixMe isnt isFixed
        $scope.fixMe = isFixed
        needDigest = true
      if scrollTop < topPos + 50 || !deployed
        animHeader()
      else $scope.$digest() if needDigest
      return

    offScrollPosition = $scope.$on 'scroll::scroll',checkPosition

    # Check state header after transition
    $timeout ->
      animHeader()
    , $rootScope.transitionDelay, false

    $scope.$on '$destroy', ->
      offScrollPosition()

    return
