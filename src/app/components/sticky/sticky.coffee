angular.module "aesop"
.directive 'sticky', ['$window', '$rootScope', ($window, $rootScope) ->

  linkFn = ($scope, $elem, $attrs) ->

    linkMe = ->
      mediaQuery = undefined
      elem = undefined
      $window = undefined
      $body = undefined
      doc = undefined
      initialCSS = undefined
      isPositionFixed = undefined
      isSticking = false
      stickyLine = undefined
      stickyBottomLine = undefined
      offset = undefined
      prevOffset = undefined
      parentHeight = undefined

      onResize = ->
        initHeight()
        initialCSS.offsetWidth = elem.offsetWidth
        unstickElement('top')
        init()
        if isSticking
          $elem.css
          position: 'relative'
          bottom: 'initial'
          top: 'initial'
        return

      onDestroy = ->
        $window.off 'scroll', checkIfShouldStick
        $window.off 'resize', onResize
        offDataReady()
        return

      # Methods
      #

      checkIfShouldStick = ->
        scrollTop = undefined
        shouldStick = undefined
        scrolledDistance = undefined

        scrolledDistance = window.pageYOffset or doc.scrollTop
        scrollTop = scrolledDistance - (doc.clientTop or 0)
        shouldStick = scrollTop >= stickyLine and scrollTop <= stickyBottomLine

        # Switch the sticky mode if the element crosses the sticky line
        if shouldStick and !isSticking
          stickElement()
        else if !shouldStick and isSticking
          # could probably do this better
          from = undefined
          compare = undefined
          closest = undefined
          compare = [
            stickyLine
            stickyBottomLine
          ]
          closest = getClosest(compare, scrollTop)
          # Check to see if we are closer to the top or bottom confines
          # and set from to let the unstick element know the origin
          if closest == stickyLine
            from = 'top'
          else if closest == stickyBottomLine
            from = 'bottom'
          unstickElement from, scrollTop
        return

      # Simple helper function to find closest value
      # from a set of numbers in an array

      getClosest = (array, num) ->
        i = 0
        minDiff = 1000
        ans = undefined
        for i of array
          m = Math.abs(num - (array[i]))
          if m < minDiff
            minDiff = m
            ans = array[i]
        ans

      stickElement = ->
        rect = undefined
        absoluteLeft = undefined
        rect = $elem[0].getBoundingClientRect()
        absoluteLeft = rect.left
        initialCSS.offsetWidth = elem.offsetWidth
        isSticking = true
        # Fix bug of ghost element after page transition
        $elem.parent().hide().show(0)
        $elem.css(
          width: elem.offsetWidth + 'px'
          position: 'fixed'
          left: absoluteLeft
          'margin-top': 0
        )
        $elem.css('top', offset + 'px')

        return

      # Passing in scrolltop and directional origin to help
      # with some math later

      unstickElement = (fromDirection) ->
        isSticking = false
        if fromDirection == 'top'
          $elem.css(
            'width': ''
            'top': initialCSS.top
            'position': initialCSS.position
            'left': initialCSS.cssLeft
            'margin-top': initialCSS.marginTop
          )
        else if fromDirection == 'bottom'
          # make sure we are checking to see if the element is confined to the parent
          $elem.css(
            width: ''
            top: ''
            bottom: 0
            position: 'absolute'
            left: initialCSS.cssLeft
            'margin-top': initialCSS.marginTop
            'margin-bottom': initialCSS.marginBottom
          )

        return

      _getTopOffset = (element) ->
        pixels = 0
        if element.offsetParent
          loop
            pixels += element.offsetTop
            element = element.offsetParent
            unless element
              break
        pixels

      init = ->
        prevOffset = _getTopOffset(elem)
        $elem.parent().css 'position': 'relative'
        stickyLine = prevOffset - offset
        stickyBottomLine = parentHeight - $elem.height() + stickyLine
        checkIfShouldStick()
        return

      initHeight = ->
        el = angular.element($elem).parent()
        parentHeight = el.parent().height()
        el.height(parentHeight)

      initHeight()
      $window = angular.element(window)
      unless $rootScope.isTouch
        isPositionFixed = false
        isSticking = false
        # elements
        $body = angular.element(document.body)
        elem = $elem[0]
        doc = document.documentElement
        # attributes
        mediaQuery = $attrs.mediaQuery or false
#        offset = if typeof $attrs.offset == 'string' then parseInt($attrs.offset.replace(/px;?/, '')) else 0
        offset = 118
        # initial style
        initialCSS =
          top: $elem.css('top')
          width: $elem.css('width')
          position: $elem.css('position')
          marginTop: $elem.css('margin-top')
          cssLeft: $elem.css('left')

        # Listeners
        #
        $scope.$on 'scroll::scroll', checkIfShouldStick
        offResize = $window.on 'resize', ->
          onResize()
        $scope.$on '$destroy', ->
          onDestroy()

        init()

    offDataReady = $rootScope.$on 'dataReady', linkMe
    return

  {
  restrict: 'A'
  link: linkFn
  }
]