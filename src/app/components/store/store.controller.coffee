angular.module "aesop"
  .controller "StoreCtrl", ($scope, $rootScope, Restangular, $stateParams, $uiViewScroll, $document, $timeout, $window, ngDialog, layout, $state) ->

    # TODO merge layout with mobile?
    layouts = [
      #materials
      [1,1,3,1]
      [3,3,0,0]
      [3,1,1,0,0]
      [0,1,1,0,0,1,1,0,0,1]
      [0,1,0,0,1,0,0,0]
      [1,0,0,1,0,0,1]
      [1,1,1,1,1]
      [1,1,3,1]
      [1,0,0,1,1,0,0,1,1,0]
      [3,0,0,0,0,0,0]
      [0,0,3,1,0,1,1,1,1,1,0]
      [0,0,1,0,0,1,1,3,1,1,0,0]
      [0,1,0,1,0,0,1,1,1,0,0,1,0,0]
      [0,0,0,0,1,1,1,1,0,0,0]
      [1,0,0,0,1,0,1]
      [1,1,1,1,0,0]
      [1,1,1,1,1]
      [1,0,0,1,1,1]
      [0,0,0,0,0]
      [0,0,1,1,0,0,1,1,1,1,0,1,0]
      [1,1,0,1,0,0,1,0,0,0,1,1,1]
      #features
      [3,1,1,1,4,2,3,3]
      [4,1,1,3,1,1,1,4,2,3,1]
      [4,1,1,2,2,1,1,4,3,3]
      [3,1,1,1,4,2,2,2,1,3,3]
      [1,1,2,3,3,4,2,2,1,1,1,1]
      [1,1,3,1,3,1,1,1]
      [2,1,1,2,3,1,1]
      [4,2,3,1,1]
      [2,4,3,3]
      [1,1,1,1,4,3]
      [1,2,1,3,3,1]
      [1,1,2,1,1,2,1,1]
      [4,1,1,2,2]
      [2,3,3,2,2]
    ]

    $scope.mainWrapper = []
    $scope.section = $stateParams.section
    $scope.store   = $stateParams.store
    $scope.count = 0
    $rootScope.page = 'page-store'

    $scope.centerMode = if $rootScope.isDesktop then 'true' else 'false'

    # Open gallery dialog
    gallery = []
    $scope.openGallery = (num) ->
      # Start gallery at the current image selected
      num--
      nbPhotos = gallery.length
      newGal = []
      while nbPhotos
        newGal.push(gallery[num])
        num++
        num = 0 if num >= gallery.length
        nbPhotos--

      $scope.gallery = newGal
      ngDialog.open
        template: 'gallery'
        scope: $scope
        className: 'dialog'

      return

    reorderList = (response) ->
      sections = response.layout.items
      toDisplay = []
      nav = []
      inspireIndex = 1
      photoIndex = 1
      for section in sections
        # Remove hidden sections
        unless section.hidden
          if section.type is 'inspire'
            # Add inspires photos links
            section.index = inspireIndex
            photo = response['inspire_photo'+photoIndex]
            # Inconsistency from server layout say there is an image, but there is not
            if photo
              photo = window.mediaServer+'/media/'+photo
              # TODO Use one object and filter in template
              section.photo = photo
              uncropped = response['inspire_photo'+photoIndex+'_uncropped']
              if uncropped
                photo = window.mediaServer+'/media/'+uncropped


              if response.video isnt 'None' and response.video isnt '' and inspireIndex is 1
                section.type = 'video'
                photoIndex--
              else
                gallery.push
                  src: photo
                  share: window.serverName+'/share/store/'+response.id+'/?image='+window.mediaServer+'/media/'+uncropped
              toDisplay.push section

            inspireIndex++
            photoIndex++

          else
            # Add sub navigation
            tempType = section.type
            tempType = 'introduction' if tempType is 'intro'
            nav.push
              'title': tempType
              'url': section.type

            toDisplay.push section

      $scope.gallery = gallery
      $rootScope.nav = nav
      $scope.sectionToDisplay = toDisplay

      return

    $scope.mail = (share)->
      $window.open('mailto:?subject=Aesop '+$rootScope.storeName+'&body='+share,
        'Aesop '+$rootScope.storeName,'left=20,top=20,width=500,height=500,toolbar=1,resizable=0')
      false

    mapScope = (response, properties, isMedia) ->
      for property in properties
        p = response[property]
        if p isnt 'None'
          p = window.mediaServer+'/media/'+p if isMedia
          $scope[property] = p
      return

    scrollToSection = ->
      # Material and feature are in the same section
      section = switch $stateParams.section
        when 'material', 'feature' then 'features_materials'
        when 'quote' then 'quote'
        else ''

      section = angular.element('#'+section)
      # Check if data was ready at the time of the transition
      if section.length > 0
        # 118 = fixHeader height, no need to be calculated each time
        $document.duScrollTo(section,118, 0)
      else
        $document.duScrollTo(0,0,0)
      return

    getContent = ->
      $scope.mainWrapper = []
      Restangular.all('store').get($stateParams.store).then (response) ->
        reorderList(response)
        # Mapping header's properties
        $rootScope.storeName = response.title
        $rootScope.storeLocation = response.subtitle
        # Maping body's properties
        mapScope response, [
          'intro_description'
          'text_copy1'
          'text_copy2'
          'designer_title'
          'latitude'
          'longitude'
          'street_address'
          'az_link'
          'az_title'
          'az_description'
          'video'
          'quotes'
          'aesop_com_store_detail_link'
        ]
        if $scope.az_title is null
          $scope.az_title = $scope.storeLocation
        mapScope response, [
          'location_photo1'
          'location_photo2'
          'intro_photo'
        ], true
        # Special mapping
        if response.further_information.length > 0
          $scope.further_informations = response.further_information
        if $scope.quotes
          $scope.quote = response.quotes[0].title
          $scope.author = response.quotes[0].subtitle
        if response.designer_image
          $scope.designer_title = response.designer_image.title
          $scope.designer_image = window.mediaServer + '/media/'+ response.designer_image.image_duotone if response.designer_image.image_duotone
        $scope.designer_url = $state.href('designer',{'designer': response.designer.id })

        for comb in response.images.combinations
          if comb?
            # 24 is the combination id of the first materials
            layer = if comb.combination_id is 100 then null else layouts[comb.combination_id - 24]

            # Use layout factory
            $scope.mainWrapper = layout.createLayout
              items: comb.items
              layout: layer
              page: 'store'
              mainWrapper : $scope.mainWrapper

        # Wait for angular, DOM and transition to calculate the position
        $timeout ->
          # This can wait: sticky element, map
          $rootScope.$broadcast 'dataReady'
          return
        , 2000, false
        return


      Restangular.one('site').get().then (response) ->
        $rootScope.translations = response

    offRefreshTranslation = $rootScope.$on 'refreshTranslation', getContent

    getContent()
    $timeout scrollToSection, $rootScope.transitionDelay, false

    $scope.$on '$destroy', ->
      offRefreshTranslation()
    return
